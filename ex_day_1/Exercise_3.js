//ORDINAL NUMBERS

// Create program that outputs competitors placements with following way: ['1st competitor was
// Julia', '2nd competitor was Mark', '3rd competitor was Spencer', '4th competitor
// was Ann', '5th competitor was John', '6th competitor was Joe']

const competitors = ['Julia', "Mark", "Spencer", "Ann" , "John", "Joe"]; 
const ordinals = ['st', 'nd', 'rd', 'th'];

//function to get ordinals. if ordinals elem is undefined, returns "th"
getOrdinal = (elem) => ordinals[elem] ? ordinals[elem] : ordinals[3];
//                                               placement                          ordinal from function                                name
const placements = competitors.map(elem => `${competitors.indexOf(elem) + 1} ${getOrdinal(competitors.indexOf(elem))} competitor was ${elem}`);

console.log(placements);

