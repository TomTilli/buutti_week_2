//RANDOMIZE ORDER OF ARRAY

const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];

const mySet = new Set();

//make a set with numbers from 0-9 in random order
while(mySet.size < array.length){ 
    mySet.add(Math.ceil(Math.random() * array.length - 1));
};

//convers set to array (i could not find how to reach for a single element from a set)
const mySetAsArray = [...mySet];

//a new array with the elements of original array in the order os mySets values
const myNewArray = array.map(elem => array[mySetAsArray[array.indexOf(elem)]]);

console.log(myNewArray);
