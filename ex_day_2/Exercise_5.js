//CHECK THE EXAM

const checkExam = (arrCorrect, arrSubmit) => {
    let result = 0;
    
    for(let i = 0 ; i < arrCorrect.length ; i++){
        if(arrSubmit[i] === arrCorrect[i]){
            result += 4;
        }else if(arrSubmit[i] === ""){
            //the resulting score does not change
        }else{
            result -= 1;
        }
    };
    if(result < 0) result = 0;

    return result;
};



console.log(checkExam(["a", "a", "b", "b"], ["a", "c", "b", "d"])); //→ 6
console.log(checkExam(["a", "a", "c", "b"], ["a", "a", "b", ""])); //→ 7
console.log(checkExam(["a", "a", "b", "c"], ["a", "a", "b", "c"])); //→ 16
console.log(checkExam(["b", "c", "b", "a"], ["", "a", "a", "c"])); //→ 0
