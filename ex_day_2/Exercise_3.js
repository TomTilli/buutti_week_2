//ABOVE AVERAGE

aboveAverage = (arr) => {
    // count average
    const arrAverage = arr.reduce((average, elem) => average + elem)/arr.length;
    //make new array same lanegth as given with higher numbers than the average (max 10 above)
    return arr.map(elem =>  elem = Math.round(Math.random() * 10 + arrAverage));
};
    

const newArr = aboveAverage([1, 5, 9, 3, 4, 5]);
console.log(newArr);