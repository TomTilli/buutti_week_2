//CALCULATOR

const calculator = (operator, num1, num2) => {
    if (operator === "+"){
        return num1 + num2;
    }else if (operator === "-"){
        return num1 - num2;
    }else if (operator === "*"){
        return num1 * num2;
    }else if (operator === "/"){
        return num1 / num2;
    }else{
        console.log("This is not a viable operator");
    };
};

const result = calculator("/", 6, 3);

console.log(result);