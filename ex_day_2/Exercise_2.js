//LIKES

const like = (nameArr) => {
    if (nameArr.length === 0){
        console.log("No one likes this.");
    }else if (nameArr.length === 1){
        console.log(`${nameArr[0]} likes this.`);
    }else if (nameArr.length === 2){
        console.log(`${nameArr[0]} and ${nameArr[1]}like this.`);
    }else if (nameArr.length === 3){
        console.log(`${nameArr[0]}, ${nameArr[1]} and ${nameArr[2]} like this.`);
    }else{
        console.log(`${nameArr[0]}, ${nameArr[1]} and ${nameArr.length - 2} others like this.`);
    };
};

const arr = ["Mikko","Kalle","Milla", "Heikki", "Pekka", "Laura"];
like(arr);