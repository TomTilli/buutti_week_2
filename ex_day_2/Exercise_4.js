//COUNT VOWELS

const getVowelCount = (str) => {
    const vowelArr = ["a", "e", "i", "o", "u", "y"];
    const newArr = [...str.toLowerCase()];

    //replace vowels with 1 and consonants with 0
    const vowelCount = newArr.map((elem) => vowelArr.some(letter => letter === elem) ? elem = 1 : elem = 0); //1/0
    //add all elements together
    console.log(vowelCount.reduce((reducer, elem) => reducer + elem));

    //testing
    // console.log(vowelCount);
    // console.log(newArr);

};

getVowelCount('abracadabra yhuo')